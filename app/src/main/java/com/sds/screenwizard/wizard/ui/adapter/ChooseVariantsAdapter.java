package com.sds.screenwizard.wizard.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sds.screenwizard.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseVariantsAdapter extends RecyclerView.Adapter<ChooseVariantsAdapter.ViewHolder> {

    private final List<String> variants;
    private final Context context;
    private final OnVariantClickListener onVariantClickListener;

    public ChooseVariantsAdapter(Context context, List<String> variants, OnVariantClickListener onVariantClickListener) {
        this.variants = variants;
        this.context = context;
        this.onVariantClickListener = onVariantClickListener;
    }

    @NonNull
    @Override
    public ChooseVariantsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_choose_variants, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseVariantsAdapter.ViewHolder holder, int position) {
        holder.variant.setText(variants.get(position));
        holder.itemView.setOnClickListener(view -> onVariantClickListener.onClick(variants.get(holder.getAdapterPosition())));
    }

    @Override
    public int getItemCount() {
        return variants.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.variant)
        TextView variant;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
