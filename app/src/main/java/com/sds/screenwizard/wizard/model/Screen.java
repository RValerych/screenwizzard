package com.sds.screenwizard.wizard.model;

import java.util.ArrayList;

public class Screen {

    private int screenId;
    private String title;
    private ArrayList<String> chooseVariants;
    private boolean isMandatory;
    private int nextScreenId;

    public int getScreenId() {
        return screenId;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<String> getChooseVariants() {
        return chooseVariants;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public int getNextScreenId() {
        return nextScreenId;
    }
}
