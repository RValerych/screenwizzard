package com.sds.screenwizard.wizard.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sds.screenwizard.R;
import com.sds.screenwizard.WizardApplication;
import com.sds.screenwizard.wizard.presenter.WizardPresenter;
import com.sds.screenwizard.wizard.ui.adapter.ChooseVariantsAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChooseVariantsFragment extends Fragment {

    private static final String VARIANTS_KEY = "VARIANTS_KEY";

    @BindView(R.id.variants)
    RecyclerView variantsView;

    @Inject
    WizardPresenter wizardPresenter;

    public static ChooseVariantsFragment getInstance(ArrayList<String> variants) {
        ChooseVariantsFragment chooseVariantsFragment = new ChooseVariantsFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(VARIANTS_KEY, variants);
        chooseVariantsFragment.setArguments(bundle);
        return chooseVariantsFragment;

    }

    protected void inject() {
        ((WizardApplication) getActivity().getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_variants, container, false);
        ButterKnife.bind(this, view);
        inject();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            List<String> variants = getArguments().getStringArrayList(VARIANTS_KEY);
            variantsView.setLayoutManager(new LinearLayoutManager(getActivity()));
            variantsView.setAdapter(new ChooseVariantsAdapter(getActivity(), variants, wizardPresenter.getOnVariantClickListener()));
        }

    }
}
