package com.sds.screenwizard.wizard.presenter;

import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sds.screenwizard.wizard.model.Screen;
import com.sds.screenwizard.wizard.ui.activity.WizardView;
import com.sds.screenwizard.wizard.ui.adapter.OnVariantClickListener;
import com.sds.screenwizard.wizard.ui.fragment.ChooseVariantsFragment;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class WizardPresenter {

    private WizardView view;
    private List<Screen> screenData;
    private final Map<Integer, String> results = new HashMap<>();
    private int currentScreen;
    private int screenCounter;

    @Inject
    public WizardPresenter() {

    }

    public void setView(WizardView view) {
        this.view = view;
    }

    public OnVariantClickListener getOnVariantClickListener() {
        return variant -> {
            results.put(currentScreen, variant);
            showNextScreen();
        };
    }

    public void setInputJson(String json) {
        parseInputJson(json);
        showNextScreen();
    }

    private void showNextScreen() {
        if (view != null && screenData != null && !screenData.isEmpty()) {
            if (screenCounter == screenData.size()) {
                view.showResults(results);
            } else {
                currentScreen = screenData.get(screenCounter).getScreenId();
                view.showNewScreen(ChooseVariantsFragment.getInstance(screenData.get(screenCounter).getChooseVariants()), screenData.get(screenCounter).getTitle());
            }
            screenCounter++;
        }
    }

    private void parseInputJson(String json) {
        Type listType = new TypeToken<List<Screen>>(){}.getType();
        screenData = new Gson().fromJson(json, listType);
    }


    public void onViewDestroyed() {
        view = null;
    }
}
