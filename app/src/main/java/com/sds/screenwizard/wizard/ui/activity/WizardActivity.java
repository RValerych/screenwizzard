package com.sds.screenwizard.wizard.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.widget.TextView;

import com.sds.screenwizard.R;
import com.sds.screenwizard.WizardApplication;
import com.sds.screenwizard.wizard.presenter.WizardPresenter;
import com.sds.screenwizard.wizard.ui.fragment.ChooseVariantsFragment;

import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WizardActivity extends AppCompatActivity implements WizardView {

    private static final String INPUTJSON_KEY = "INPUTJSON_KEY";
    public static final String RESULT_KEY = "RESULT_KEY";

    @Inject
    WizardPresenter wizardPresenter;

    @BindView(R.id.title)
    TextView titleView;

    public static Intent getIntent(Context context, String inputJson) {
        Intent intent = new Intent(context, WizardActivity.class);
        intent.putExtra(INPUTJSON_KEY, inputJson);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);
        ButterKnife.bind(this);
        inject();
        wizardPresenter.setView(this);
        wizardPresenter.setInputJson(getIntent().getStringExtra(INPUTJSON_KEY));
    }

    protected void inject() {
        ((WizardApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @OnClick(R.id.back_button)
    public void onBackClick() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wizardPresenter.onViewDestroyed();
    }

    @Override
    public void showNewScreen(ChooseVariantsFragment instance, String title) {
        titleView.setText(title);
        getSupportFragmentManager().beginTransaction().add(R.id.container, instance).commit();
    }

    @Override
    public void showResults(Map<Integer, String> results) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULT_KEY, results.toString());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
