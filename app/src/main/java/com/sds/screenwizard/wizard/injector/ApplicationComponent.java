package com.sds.screenwizard.wizard.injector;


import com.sds.screenwizard.wizard.ui.activity.WizardActivity;
import com.sds.screenwizard.wizard.ui.fragment.ChooseVariantsFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(WizardActivity wizardActivity);
    void inject(ChooseVariantsFragment chooseVariantsFragment);

}