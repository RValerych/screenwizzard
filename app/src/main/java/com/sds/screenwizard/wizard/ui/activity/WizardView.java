package com.sds.screenwizard.wizard.ui.activity;

import android.util.SparseArray;

import com.sds.screenwizard.wizard.ui.fragment.ChooseVariantsFragment;

import java.util.Map;

public interface WizardView {
    void showNewScreen(ChooseVariantsFragment instance, String title);

    void showResults(Map<Integer, String> results);

}
