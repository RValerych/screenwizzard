package com.sds.screenwizard.wizard.ui.adapter;

public interface OnVariantClickListener {

    void onClick(String variant);

}
