package com.sds.screenwizard;

import android.app.Application;

import com.sds.screenwizard.wizard.injector.ApplicationComponent;
import com.sds.screenwizard.wizard.injector.ApplicationModule;
import com.sds.screenwizard.wizard.injector.DaggerApplicationComponent;


public class WizardApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
