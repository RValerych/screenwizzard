package com.sds.screenwizard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sds.screenwizard.wizard.ui.activity.WizardActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.result)
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        startActivityForResult(WizardActivity.getIntent(this, getInputJson()), 100);
    }

    private String getInputJson() {
        return "[\n" +
                "\t{\n" +
                "\t    \"screenId\": 1,\n" +
                "\t\t\"title\": \"What kind of flue do you have?\",\n" +
                "\t\t\"chooseVariants\": [\"Dry\", \"Wet\"],\n" +
                "\t\t\"isMandatory\": true\n" +
                "\t},\n" +
                "\t{\n" +
                "\t    \"screenId\": 2,\n" +
                "\t\t\"title\": \"Does your snuffle chronic or not?\",\n" +
                "\t\t\"chooseVariants\": [\"Yes\", \"No\"],\n" +
                "\t\t\"isMandatory\": true\n" +
                "\t}\n" +
                "]";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            result.setText(data.getStringExtra(WizardActivity.RESULT_KEY));
        }
    }
}
